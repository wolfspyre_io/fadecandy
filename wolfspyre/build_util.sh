#!/bin/bash
# Wolfspyre Helper script. 11/1/16
error () {
  MSG=$1
  echo -e "\033[31mERROR\033[0m: $MSG" >>/dev/stderr
}

warn () {
  MSG=$1
  echo -e "\033[1;33mWARN\033[0m: $MSG" >>/dev/stderr
}

info () {
  MSG=$1
  echo -e "\033[32mINFO\033[0m: $MSG"
}

debug () {
  MSG=$1
  if [[ $2 && $2 == 'verbose' ]]; then
    verbose_msg=true
  else
    verbose_msg=false
  fi
  if [ -n  "$DEBUG" ]; then
    if $verbose_msg; then
      #only display verbose if DEBUG == verbose
      if [ ${DEBUG} == 'verbose' ]; then
        echo -e "\033[35mVERBOSE\033[0m: \033[32m${FUNCNAME[1]}()\033[0m: $MSG"
      fi
    else
      echo -e "\033[34mDEBUG\033[0m: \033[32m${FUNCNAME[1]}()\033[0m: $MSG"
    fi
  fi
}

backup_file(){
  file=$1
  if [[ -d /root/backup ]]; then
    debug "/root/backup exists"
  else
    mkdir /root/backup
  fi
  if [ -f $file ]; then
    debug "${file} exists. Backing up to /root/backup/${file}.${DATESTRING}"
    FILE=`/bin/echo ${file}| /bin/sed -e 's/\//_/g' -`
    cat ${file} > /root/backup${FILE}.${DATESTRING}
  else
    debug "Nothing to do. ${file} does not exist"
  fi

}
spinner()
{
  #http://stackoverflow.com/questions/12498304/using-bash-to-display-a-progress-working-indicator
  #http://stackoverflow.com/questions/1570262/shell-get-exit-code-of-background-process
  local pid=$!
  if [[ $1 ]]; then
    local cmd=$1
    debug "using ${cmd} as the command name for `ps -p ${pid} -o cmd h`"
  else
    local cmd=`ps -p ${pid} -o cmd h`
  fi
  local delay=0.5
  local spinstr='|/-\'
  echo -ne "\033[32m"
  while [ "$(ps a | awk '{print $1}' | grep $pid)" ]; do
    local temp=${spinstr#?}
    printf "%c" "$spinstr"
    local spinstr=$temp${spinstr%"$temp"}
    sleep $delay
    printf "\b\b\b"
  done
  wait $pid
  local pidstatus=$?
  debug "pid: ${pid} returned status: ${pidstatus}" verbose
  printf "\b\b"
  echo -ne "\033[0m"
  case ${pidstatus} in
    0 )
    debug "${cmd} exited successfully"
    ;;
    100 )
    #apt-get returns this when it's installed a package
    error "${cmd} could not install something? "
    exit 1
    ;;
    * )
    #assume something went wrong
    error "The execution of [ ${cmd} ] failed!"
    exit 1
    ;;
  esac
}

check_installed(){
  package=$1
  if [ `/usr/bin/dpkg -l |/bin/egrep -c "^ii\s+${package}"` -gt 0 ]; then
    debug "${package} installed"
    return 1
  else
  return 0
fi
}
idempo_install(){
  check_installed $1
  installed=$?
  if [ ${installed} -eq 0 ]; then
    debug "installing $1"
    (/usr/bin/apt-get install -qq -y $1 >/dev/null $2>&1)&
    spinner
  fi
}

bulk_install(){
  argAry1=("${@}")
  pkgs_to_install=""
  installed_pkgs=""
  for pkg in ${argAry1[@]}; do
    check_installed ${pkg}
    installed=$?
    if [ ${installed} -eq 0 ]; then
      info "${pkg} Not installed. Enqueueing."
      pkgs_to_install+="${pkg} "
    else
      installed_pkgs+="${pkg} "
    fi
  done
  if [ ${#pkgs_to_install} -gt 0 ]; then
    info "Installing: ${pkgs_to_install}"
    (/usr/bin/apt-get -qq -y install ${pkgs_to_install} >/dev/null $2>&1) &
    spinner
  else
    info "${installed_pkgs} are all already installed."
  fi
}
