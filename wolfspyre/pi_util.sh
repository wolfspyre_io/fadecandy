#!/bin/bash
# Wolfspyre Raspberry Pi Helper script. 11/1/16

#ensure dependent scripts have been sourced
for scripty in build_util.sh; do
  if [ -f ${scripty} ]; then
    source ${scripty}
  else
    echo "Cannot find ${scripty}. Somethings wrong!";
    exit 1
  fi
done
DATESTRING=`/bin/date +%m%d%y%H%M`


xwin_enable(){
  debug "I should enable xwin"
  #TODO: validate this. Here's the original output from disabling
  #insserv: warning: current start runlevel(s) (empty) of script `lightdm' overrides LSB defaults (2 3 4 5).
  sudo systemctl set-default graphical.target
}

xwin_disable(){
  info "Ensuring xwindows is disabled at boot"
  sudo systemctl set-default multi-user.target
}
xwin_enable_disable() {
  if [[ $PI_ENABLE_XWINDOWS ]]; then
    xwin_enable
  else
    xwin_disable
  fi
}

resize_root(){
  info "Checking root filesystem resize"
  # this is now handled for us automatically by the new pi images.

#  ROOTBLOCKS=`df /|awk '/\/dev\/root/ {print $2}'`
#  if [ ${ROOTBLOCKS} -gt 4000000 ]; then
#    debug "rootvol is ${ROOTBLOCKS} blocks. larger than threshold. resize unnecessary" fi
#  else
#    info "Root volume has not yet been resized."
#    #root@blinkie:/# fdisk -l /dev/mmcblk0 |awk '/^Disk.*mmc/ {print $7}'
#    # 60874752
#    # root@blinkie:/# sec_max=$(fdisk -l /dev/mmcblk0 |awk '/^Disk.*mmc/ {print $7}'); let last_sector=($sec_max -1); echo $last_sector
#    # 60874751
#    sec_max=$(fdisk -l /dev/mmcblk0 |awk '/^Disk.*mmc/ {print $7}')
#    first_sector=$(fdisk -l /dev/mmcblk0|awk '/Linux$/ {print $2}')
#    let last_sector=($sec_max -1)
#    debug "Max sectors: ${sec_max}. First and Last sector for root partition: ${first_sector} ${last_sector}"
#    echo "d
#2
#n
#p
#2
#${first_sector}
#${last_sector}
#w
#"|fdisk /dev/mmcblk0 >/dev/null  ; /sbin/partprobe; /sbin/resize2fs /dev/mmcblk0p2
#    error "rootvol NOT Resized."
#  fi

}
set_host(){
  HOSTNAME=$1
  DOMAINNAME=$2
  debug "got hostname/domainname: ${HOSTNAME} ${DOMAINNAME}"
  if [ `/bin/cat /etc/hostname` == ${HOSTNAME} ]; then
    debug "hostname already set"
  else
    backup_file /etc/hostname
    /bin/echo ${HOSTNAME}>/etc/hostname
  fi

  if [ `/bin/domainname` == ${DOMAIN} ]; then
    debug "domainname already set"
  else
    debug "setting domainname"
    domainname ${DOMAIN}
  fi

  backup_file /etc/hosts
  debug "updating hosts file"
  /bin/echo "127.0.0.1 ${FQDN} ${HOSTNAME} localhost" >/etc/hosts
  /bin/echo "${IP} ${FQDN} ${HOSTNAME}" sudo tee >> /etc/hosts
}
