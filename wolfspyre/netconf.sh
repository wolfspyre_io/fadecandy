#!/bin/bash
source build_util.sh
gen_net_setup(){
  if [[ $PI_IP ]]; then
    if [ $PI_IP == "Wolfspyre" ]; then
      if [[ $PI_INT ]]; then
        INT=${PI_INT}
        debug "Int ${INT} set"
      else
        INT='wlan0'
      fi
      IP='192.0.2.198'
      GATEWAY='192.0.2.1'
      NETMASK='255.255.255.0'
      DNS='192.0.2.1'
      SSID='WolfspawGFXN'
      PSK='monkeychum'
#      setup_net ${IP} ${INT} ${NETMASK} ${GATEWAY} ${DNS} ${DOMAIN} ${SSID} ${PSK}
    elif [ $PI_IP == "DHCP" ]; then
      INT=`/bin/netstat -rn| /usr/bin/awk ' $1 ~ /0.0.0.0/ {print $8}'`
      debug "Default interface: ${INT}"
      IP=`/sbin/ifconfig $INT | /usr/bin/awk '/inet addr\:/ { gsub(/addr:/,""); print $2}'`
      debug "IP ${IP} set via divination of existing config"
    else
      IP=${PI_IP}
      debug "IP ${IP} set via environment variable"
      if [[ $PI_INT ]]; then
        INT=${PI_INT}
        debug "Int ${INT} set"
      else
        INT='eth0'
        debug "Int ${INT} selected as fallback"
      fi
      if [[ $PI_NETMASK ]]; then
        NETMASK=${PI_NETMASK}
        debug "Netmask: ${NETMASK} set via environment variable"
      else
        NETMASK='255.255.255.0'
        debug "Netmask: ${NETMASK} set as fallback"
      fi
      if [[ $PI_GW ]]; then
        GATEWAY=${PI_GW}
        debug "Gateway: ${GATEWAY} set via environment variable"
      else
        GATEWAY='192.0.2.1'
      fi
      if [[ $PI_DNS ]]; then
        DNS=${PI_DNS}
        debug "DNS: ${DNS} set via environment variable"
      else
        DNS='192.0.2.1'
        debug "DNS: ${DNS} set as fallback"
      fi
      debug "manually configuring network:"
#      setup_net ${IP} ${INT} ${NETMASK} ${GATEWAY} ${DNS} ${DOMAIN}
    fi
  else
    INT=`/bin/netstat -rn| /usr/bin/awk ' $1 ~ /0.0.0.0/ {print $8}'`
    debug "Default interface: ${INT}"
    IP=`/sbin/ifconfig $INT | /usr/bin/awk '/inet addr\:/ { gsub(/addr:/,""); print $2}'`
    debug "IP ${IP} set via divination of existing config"
  fi
}

# setup_net
setup_net(){
  ip=$1
  int=$2
  netmask=$3
  gateway=$4
  dns=$5
  domain=$6
  if [ ${int} == 'wlan0' ]; then
    ssid=$7
    psk=$8
  fi
  debug "Interface: ${int} IP: ${ip} Netmask: ${netmask} DNS: ${dns}"
  backup_file /etc/network/interfaces
  debug "pruning lines matching 'iface ${int}' from /etc/network/interfaces"
  cat <<EOF>/etc/network/interfaces
auto lo
iface lo inet loopback
EOF
  if [ ${int} == 'wlan0' ]; then
    info 'wlan0 specified. Configuring wifi'
    cat <<EOF>/etc/network/interfaces
#iface eth0 inet dhcp
allow-hotplug wlan0
iface wlan0 inet manual
    address ${ip}
    netmask ${netmask}
    gateway ${gateway}
    dns-nameservers ${dns}
    dns-search ${domain}
    dns-domain ${domain}
    wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
EOF
    if [ -f /etc/wpa_supplicant/wpa_supplicant.conf ]; then
      backup_file /etc/wpa_supplicant/wpa_supplicant.conf
      rm -f /etc/wpa_supplicant/wpa_supplicant.conf
    fi
    if [ -f /etc/network/interfaces.d/eth0 ]; then
      backup_file /etc/network/interfaces.d/eth0
      rm -f /etc/network/interfaces.d/eth0
    fi
    backup_file /etc/network/interfaces.d/${int}
    rm -f /etc/network/interfaces.d/${int}
    cat <<EOF>/etc/wpa_supplicant/wpa_supplicant.conf
country=US
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
network={
    ssid="${ssid}"
EOF
    if [[ $psk ]]; then
      cat <<EOF>>/etc/wpa_supplicant/wpa_supplicant.conf
    psk="${psk}"
    key_mgmt=WPA-PSK
}
EOF
    else
      warn 'no PSK.'
      cat <<EOF>>/etc/wpa_supplicant/wpa_supplicant.conf
      key_mgmt=NONE
}
EOF
    fi
  else
    # eth or other wired
    backup_file /etc/network/interfaces.d/${int}
    cat <<EOF>/etc/network/interfaces.d/${int}
iface ${int} inet static
address ${ip}
netmask ${netmask}
gateway ${gateway}
dns-nameservers ${dns}
dns-search ${domain}
dns-domain ${domain}
EOF
  fi
  backup_file /etc/resolv.conf
  service networking restart
  ifup ${int}
#
# not a jesse thing. Purge?
#TODO: check to see how this acts on a new system
  #  /sbin/resolvconf --disable-updates
  #backup_file /etc/resolvconf/resolv.conf.d/head
  #cat <<EOF>/etc/resolvconf/resolv.conf.d/head
#nameserver ${dns}
#search ${domain}
#EOF
}
